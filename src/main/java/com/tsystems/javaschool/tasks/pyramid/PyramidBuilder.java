package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int numsCount = inputNumbers.size();
        int k = 0;

        while (numsCount > 0) {
            k++;
            numsCount -= k;

            if (numsCount < 0) {
                throw new CannotBuildPyramidException();
            }
        }
        System.out.println("Pyramid can be build!");

        Collections.sort(inputNumbers);

        int lines = k;
        int columns = k + k - 1;
        int[][] pyramid = new int[lines][columns];

        k = 0;
        for (int i = 0; i < lines; i++) {
            for (int j = 0; j < columns; j++) {
                if (j >= columns / 2 - i && j <= columns / 2 + i) {
                    pyramid[i][j] = inputNumbers.get(k);
                    k++;
                    j++;
                }
            }
        }

        return pyramid;
    }


}
