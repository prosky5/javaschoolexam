package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (statement == null || statement.isEmpty()) return null;

        statement = statement.replaceAll(" ", "");

        if (statement.replaceAll("[0-9+\\-*/.()]", "").length() > 0) {
            System.out.println("--Statement contain illegal characters!");
            return null;
        }

        if (isDuplicate(statement)) {
            System.out.println("--Statement contain illegal duplicates!");
            return null;
        }

        if (!isBalanced(statement)) {
            System.out.println("--Count of parentheses in statement is not balanced!");
            return null;
        }

        return new Delegator().delegateTasks(statement);
    }

    private boolean isDuplicate(String s) {
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if ((chars[i] == '+' || chars[i] == '-' || chars[i] == '*' || chars[i] == '/' || chars[i] == '.')
                    && (chars[i+1] == '+' || chars[i+1] == '-' || chars[i+1] == '*' || chars[i+1] == '/' || chars[i+1] == '.')) {
                return true;
            }
        }
        return false;
    }

    private boolean isBalanced(String s) {
        String str = s.replaceAll("[0-9+\\-*/. ]", "");
        Stack<Character> stack = new Stack<>();

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                stack.push(str.charAt(i));
            } else if (str.charAt(i) == ')') {
                if (stack.isEmpty()) return false;
                if (stack.pop() != '(') return false;
            }
        }
        return stack.isEmpty();
    }
}
