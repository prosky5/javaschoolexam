package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Delegator {

    String delegateTasks(String s) {
        s = recurParentheses(s);

        List<String> elements = new ArrayList<>();
        List<Character> operators = new ArrayList<>();

        sortSymbols(s, elements, operators);

        s = new Calculations().calculate(s, elements, operators);
        if (s == null) {
            return null;
        }

        s = new BigDecimal(s).setScale(4, BigDecimal.ROUND_HALF_UP).toString();

        while (s.contains(".") && (s.endsWith("0") || s.endsWith("."))) {
            s = s.substring(0, s.length() - 1);
        }

        return s;
    }

    private void sortSymbols(String s, List<String> elements, List<Character> operators) {
        elements.addAll(Arrays.asList(s.split("(?<=\\d)[*/+-]" )));

        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == '/' || s.charAt(i) == '*' || s.charAt(i) == '+') {
                operators.add(s.charAt(i));
            }
            if (s.charAt(i) == '-' && s.substring(i - 1, i).matches("[\\d]")) {
                operators.add(s.charAt(i));
            }
        }
    }

    String recurParentheses(String s) {
        if (s.contains("(")) {
            int start = 0;
            int end = s.length() - 1;

            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '(') {
                    start = i;
                    break;
                }
            }
            for (int i = s.length() - 1; i >= 0; i--) {
                if (s.charAt(i) == ')') {
                    end = i;
                    break;
                }
            }
            s = s.substring(0, start) + delegateTasks(s.substring(start + 1, end)) + s.substring(end + 1);
        }
        return s;
    }
}