package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;

public enum Operations
{
    PLUS {
        public BigDecimal apply(BigDecimal x, BigDecimal y) { return x.add(y); }
    },
    MINUS {
        public BigDecimal apply(BigDecimal x, BigDecimal y) { return x.subtract(y); }
    },
    TIMES {
        public BigDecimal apply(BigDecimal x, BigDecimal y) { return x.multiply(y); }
    },
    DIVIDE {
        public BigDecimal apply(BigDecimal x, BigDecimal y) { return x.divide(y, 10, BigDecimal.ROUND_HALF_UP); }
    };

    public abstract BigDecimal apply(BigDecimal x, BigDecimal y);

}