package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.List;

class Calculations {

    public String calculate(String s, List<String> elements, List<Character> operators) {

        while (operators.size() > 0) {

            for (int i = 0; i < operators.size(); i++) {

                if (operators.get(i) == '*') {
                    elements.set(i, Operations.TIMES.apply(new BigDecimal(elements.get(i)), new BigDecimal(elements.get(i + 1))).toString());
                    elements.remove(i + 1);
                    operators.remove(i);
                    i--;
                    continue;
                }
                if (operators.get(i) == '/') {
                    if (elements.get(i + 1).equals("0")) {
                        return null;
                    }
                    elements.set(i, Operations.DIVIDE.apply(new BigDecimal(elements.get(i)), new BigDecimal(elements.get(i + 1))).toString());
                    elements.remove(i + 1);
                    operators.remove(i);
                    i--;
                }
            }
            for (int i = 0; i < operators.size(); i++) {
                if (operators.get(i) == '+') {
                    elements.set(i, Operations.PLUS.apply(new BigDecimal(elements.get(i)), new BigDecimal(elements.get(i + 1))).toString());
                    elements.remove(i + 1);
                    operators.remove(i);
                    i--;
                    continue;
                }
                if (operators.get(i) == '-') {
                    elements.set(i, Operations.MINUS.apply(new BigDecimal(elements.get(i)), new BigDecimal(elements.get(i + 1))).toString());
                    elements.remove(i + 1);
                    operators.remove(i);
                    i--;
                }
            }

        }
        return elements.get(0);
    }
}