package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) throw new IllegalArgumentException();

        for (Object letter : y) {
            if (x.size() > 0 && x.get(0).toString().equals(letter.toString())) {
                x.remove(0);
            }
        }
        return x.size() <= 0;
    }
}
